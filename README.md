# TP3 - A simple search engine



## Installation

Use pip to install requirements.txt and get necessary packages


`

Add your urls in the data folder with the name urls.json

## Usage

`bash
python3 main.py "your word"
`

## Tests

You can run tests with package unittest
