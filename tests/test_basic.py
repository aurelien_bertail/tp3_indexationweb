# -*- coding: utf-8 -*-

import unittest
from nltk.tokenize import word_tokenize
import json
import pandas as pd

class TestSearchEngine(unittest.TestCase):
    """
    Test class for the search engine functions
    """

    def test_read_request(self):
        """
        Test the read_request function
        """
        request = "hello world"
        result = read_request(request)
        self.assertEqual(result, "hello world")
        
    def test_tokenize_request(self):
        """
        Test the tokenize_request function
        """
        request = "hello world"
        result = tokenize_request(request)
        self.assertEqual(result, ["hello", "world"])

    def test_filter_docs(self):
        """
        Test the filter_docs function
        """
        index = {'hello': [1, 2, 3], 'world': [2, 3, 4]}
        tokens = ['hello', 'world']
        result = filter_docs(index, tokens)
        self.assertEqual(result, [2, 3])

    def test_rank_docs(self):
        """
        Test the rank_docs function
        """
        ids = [1, 2, 3]
        index = {'hello': {1: {'count': 2}, 2: {'count': 3}, 3: {'count': 1}}, 'world': {1: {'count': 1}, 2: {'count': 2}, 3: {'count': 2}}}
        tokens = ['hello', 'world']
        result = rank_docs(ids, index, tokens)
        self.assertEqual(result, [2, 3, 1])

    def test_write_results(self):
        """
        Test the write_results function
        """
        sorted_ids = [2, 3, 1]
        documents = {1: "document 1", 2: "document 2", 3: "document 3"}
        write_results(sorted_ids, documents)

        with open("results/results.json", "r") as f:
            results = json.load(f)

        self.assertEqual(results, {0: "document 2", 1: "document 3", 2: "document 1"})

if __name__ == '__main__':
    unittest.main()
