from nltk.tokenize import word_tokenize
import json
import pandas as pd

def read_request(request):
    """
    Reads the user's request

    Args:
        request: the user request

    Returns:
        the request in a string

    """
    return str(request)

def tokenize_request(request):
    """
    Tokenize the user's request

    Args:
        request: the user request as a string

    Returns:
        the request as a list of token

    """
    tokens=list(request.lower().split(" "))
    return tokens

def filter_docs(index, tokens):
    """
    Filter the docs which has all the tokens

    Args:
        tokens: the list of tokens in the request

    Returns:
        the documents that has all tokens as a list

    """
    try :
        intersection = list(index[tokens[0]])
    except KeyError :
        intersection = []

    if len(tokens) > 1 :
        for token in tokens[1:] :
            inter2 = index[token]
            intersection = [x for x in inter2 if x in intersection]

    return intersection
    

def rank_docs(ids, index, tokens):
    """
    Rank the docs in order by most tokens

    Args:
        docs: the documents to rank
        index: the index used to sort
        tokens: the tokens to look for in the titles

    Returns:
        the documents ranked as list

    """
    print(ids)
    print(len(ids))
    unsorted_ids = pd.DataFrame({'id':ids,'value':[0 for i in range(len(ids))]})

    for doc in ids :
        somme = 0
        for token in tokens:
            somme = somme + index[token][doc]['count']
        unsorted_ids[unsorted_ids.id == doc].value = somme

    sorted_ids = unsorted_ids.sort_values(by='value', ascending=False)
    return list(sorted_ids.id)

def write_results(sorted_ids, documents):
    """
    Write the documents in a JSON

    Args:
        sorted_ids: the ids, sorted by the rank
        
        documents: the documents to match with
    """
    documents_dic = {}

    for i in range(len(sorted_ids)) :
        doc = sorted_ids[i]
        documents_dic[i] = documents[int(doc)]

    with open("results/results.json", "w") as f:
        json.dump(documents_dic, f)
    f.close