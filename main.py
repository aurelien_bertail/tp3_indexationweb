import json
import argparse
from pathlib import Path

# functions

from request_treatment.auxiliaries import *

def main(request):
    with open("data/documents.json", "r") as f:
        documents = json.load(f)
        
    with open("data/index.json", "r") as f:
        index = json.load(f)

    req = read_request(request)
    tokens = tokenize_request(req)
    docs = filter_docs(index, tokens)
    sorted_docs = rank_docs(docs, index, tokens)
    write_results(sorted_docs, documents)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog = 'SimpleWebIndex',
                    description = 'Builds a simple webindex out of an url json file',
                    epilog = 'Good luck, sometimes it works.')

    parser.add_argument('request')

    args = parser.parse_args()
    
    main(args.request)